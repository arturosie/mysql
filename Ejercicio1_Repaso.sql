DROP DATABASE IF EXISTS Ejercicio1_Repaso;

/* CREATE DATABASE AND TABLES*/
CREATE DATABASE Ejercicio1_Repaso;
USE Ejercicio1_Repaso;
CREATE TABLE PERSONA(nombre VARCHAR(20), apellidos VARCHAR(20), trabajo VARCHAR (30), PRIMARY KEY(nombre, apellidos));
CREATE TABLE SITUACION(hora TINYINT UNSIGNED, lugar VARCHAR(30), nombre_PERSONA VARCHAR(20), apellidos_PERSONA VARCHAR(20), vestuario VARCHAR(20), mercancia VARCHAR(20), PRIMARY KEY(hora), FOREIGN KEY(nombre_PERSONA, apellidos_PERSONA) REFERENCES PERSONA(nombre, apellidos));
CREATE TABLE OBJETO(nombre VARCHAR(20), tamaño ENUM('Pequeño', 'Mediano', 'Grande'), nombre_OBJETO_contenedor VARCHAR(20), PRIMARY KEY(nombre), FOREIGN KEY(nombre_OBJETO_contenedor) REFERENCES OBJETO(nombre));
CREATE TABLE lleva(hora_SITUACION TINYINT UNSIGNED, nombre_OBJETO VARCHAR(20), FOREIGN KEY(hora_SITUACION) REFERENCES SITUACION(hora), FOREIGN KEY(nombre_OBJETO) REFERENCES OBJETO(nombre));
/*END CREATE DATABASE AND TABLES*/


/* DATES */
INSERT INTO PERSONA VALUES('Juan', 'Sánchez', 'Mecanico');
INSERT INTO PERSONA VALUES('Pele', 'Fernandez', 'Limpiador');
INSERT INTO PERSONA VALUES('Alvaro', 'Perez', 'Secretario');
INSERT INTO PERSONA VALUES('Oscar', 'Ruiz', 'Informatico');
INSERT INTO PERSONA VALUES('Jose Maria', 'De la Osa', 'Encargado de mantenimiento');
INSERT INTO PERSONA VALUES('Miguel Angel', 'Sierra', 'Informatico');
INSERT INTO OBJETO(nombre, tamaño) VALUES('Mochila', 'Grande');
INSERT INTO OBJETO VALUES('Martillo', 'Mediano', 'Mochila');
INSERT INTO OBJETO VALUES('Pistola', 'Mediano', 'Mochila');
INSERT INTO OBJETO VALUES('Bolsa', 'Mediano', 'Mochila');
INSERT INTO OBJETO VALUES('Cuchillo', 'Pequeño', 'Bolsa');
INSERT INTO SITUACION VALUES(16, 'Taberna de MOE', 'Juan', 'Sánchez', 'Informal', '500g');
INSERT INTO SITUACION VALUES(12, 'Mercadona', 'Oscar', 'Ruiz', 'Chandal', '3kg');
INSERT INTO SITUACION VALUES(11, 'Eroski', 'Alvaro', 'Perez', 'Smoking', '100g');
INSERT INTO SITUACION VALUES(13, 'Casa de Juan', 'Jose Maria', 'De la Osa', 'Traje negro', '1kg');
INSERT INTO lleva VALUES(16, 'Martillo');
INSERT INTO lleva VALUES(12, 'Bolsa');
/* END DATES */

/* UPDATE DATE */
UPDATE PERSONA SET nombre='Pedro' WHERE nombre='Pele';
/* END UPDATE DATE */

/* SHOW TABLES */
SELECT * FROM SITUACION;
SELECT * FROM OBJETO;
SELECT * FROM PERSONA;
SELECT * FROM lleva;
/* END SHOW TABLES */
