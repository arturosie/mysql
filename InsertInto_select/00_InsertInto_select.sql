use jardineria;

/*Borra los clientes que no tengan pedidos*/
/*SELECT Pedidos.CodigoCliente AS PedidosCliente, Clientes.CodigoCliente AS ClientesCliente, Clientes.NombreCliente FROM Clientes LEFT JOIN Pedidos ON Clientes.CodigoCliente = Pedidos.CodigoCliente WHERE Pedidos.CodigoCliente is NULL GROUP BY Clientes.CodigoCliente; CONSULTA*/
START TRANSACTION;
DELETE FROM Clientes WHERE CodigoCliente NOT IN (SELECT DISTINCT CodigoCliente FROM Pedidos);
COMMIT; /*Si queremos que se realice*/
ROLLBACK; /*Si no queremos que se realice*/

/*Incrementa en un 20% el precio de los productos que no tengan pedidos*/


/*Borra los pagos del cliente con menor limite de credito*/

/*Establece a 0 el limite de credito del cliente que menos unidades pedidas tenga el producto OR179*/

