DROP DATABASE IF EXISTS Ejercicio2_Refuerzo;
CREATE DATABASE Ejercicio2_Refuerzo;
USE Ejercicio2_Refuerzo;

CREATE TABLE ACTORES(
    Codigo INT AUTO_INCREMENT,
    Nombre VARCHAR(30),
    Fecha DATE,
    NACIONALIDAD VARCHAR(20),
    CONSTRAINT PK_ACTORES PRIMARY KEY (Codigo));

CREATE TABLE PERSONAJES(
    Codigo int AUTO_INCREMENT,
    Nombre VARCHAR(20),
    Raza VARCHAR(20),
    GRADO SMALLINT UNSIGNED,
    Codigo_ACTORES INT,
    CodigoSuperior_PERSONAJES INT,
    CONSTRAINT PK_PERSONAJES PRIMARY KEY (Codigo),
    CONSTRAINT FK_FOREIGN KEY (Codigo_ACTORES) REFERENCES  ACTORES (Codigo));

CREATE TABLE NAVES(
    Codigo INT AUTO_INCREMENT,
    NºTripulantes int(11) ,Nombre VARCHAR(20) ,PRIMARY KEY (Codigo));
CREATE TABLE PELICULAS (Codigo int(11) AUTO_INCREMENT,Titulo VARCHAR(20) ,Director VARCHAR(40) ,Año year(4) ,PRIMARY KEY (Codigo));
CREATE TABLE PLANETAS (Codigo int(11)  AUTO_INCREMENT, Galaxia VARCHAR(20), Nombre VARCHAR(20),PRIMARY KEY (Codigo));
CREATE TABLE PERSONAJES_PELICULAS (Codigo_PERSONAJES int(11),Codigo_PELICULAS int(11) ,PRIMARY KEY (Codigo_PERSONAJES,Codigo_PELICULAS),FOREIGN KEY (Codigo_PERSONAJES) REFERENCES PELICULAS (Codigo));

CREATE TABLE VISITAS (Codigo_NAVES int(11), Codigo_PLANETAS int(11), Codigo_PELICULAS int(11), FOREIGN KEY (Codigo_NAVES) REFERENCES  NAVES (Codigo),FOREIGN KEY (Codigo_PLANETAS) REFERENCES  PLANETAS (Codigo), FOREIGN KEY (Codigo_PELICULAS) REFERENCES  PELICULAS (Codigo));
