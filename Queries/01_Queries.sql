use jardineria;

/*Mostrar productos que su precio este por enciama de la media*/
SELECT CodigoProducto, PrecioVenta FROM Productos WHERE PrecioVenta>(SELECT AVG(PrecioVenta) From Productos) ORDER BY PrecioVenta DESC;

/*Mostrar el nombre y el precio del producto mas caro*/
SELECT PrecioVenta, Nombre FROM Productos WHERE PrecioVenta=(SELECT MAX(PrecioVenta) FROM Productos);

/*Mostrar el nombre, apellidos y el email de los empleados a cargo de "Alberto Soria" PARTE 2*/
SELECT Nombre, CONCAT(Apellido1, ' ', Apellido2) AS NombreCompleto, Email FROM Empleados WHERE CodigoJefe=(SELECT CodigoEmpleado FROM Empleados WHERE Nombre='Alberto' AND Apellido1='Soria');

/*Mostrar codigo y nombre del cliente y del pedido que ha realizado*/
SELECT Clientes.NombreCliente, Clientes.CodigoCliente, Pedidos.CodigoCliente, Pedidos.CodigoPedido FROM Clientes, Pedidos WHERE Clientes.CodigoCliente=Pedidos.CodigoCliente;

/*Mostrar codigo y nombre del cliente y del pedido que ha realizado*/  /* LO MISMO QUE LA DE ARRIBA PERO OPTIMIZADA USANDO JOIN */
SELECT Clientes.NombreCliente, Clientes.CodigoCliente, Pedidos.CodigoPedido FROM Clientes JOIN Pedidos ON Clientes.CodigoCliente=Pedidos.CodigoCliente;

/*Mostrar codigo y nombre del cliente y del pedido que ha realizado*/  /* LO MISMO QUE LA DE ARRIBA PERO OPTIMIZADA USANDO JOIN */
SELECT Clientes.NombreCliente, Clientes.CodigoCliente, Pedidos.CodigoPedido FROM Clientes NATURAL JOIN Pedidos;

/*Mostrar Nombre del empleado y su codigo, el codigo de la oficina y su nombre y ciudad de la oficina*/

/*Mostrar el nombre, el pedido y el precio de cada cliente*/
SELECT Clientes.NombreCliente, DetallePedidos.CodigoPedido, SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad) FROM DetallePedidos, Pedidos, Clientes WHERE Pedidos.CodigoPedido=DetallePedidos.CodigoPedido AND Clientes.CodigoCliente=Pedidos.CodigoCliente GROUP BY DetallePedidos.CodigoPedido;

/*Mostrar el precio total de los pedidos de cada cliente*/
SELECT Clientes.NombreCliente, SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad) FROM DetallePedidos, Pedidos, Clientes WHERE Pedidos.CodigoPedido=DetallePedidos.CodigoPedido AND Clientes.CodigoCliente=Pedidos.CodigoCliente GROUP BY Pedidos.CodigoCliente;

/*Obtener el nombre del cliente con mayor limite de credito*/
SELECT Clientes.NombreCliente, Clientes.LimiteCredito FROM Clientes WHERE LimiteCredito=(SELECT MAX(LimiteCredito) FROM Clientes);

/*Obtener el nombre, Apellido1 y cargo de los empleados que no representen a ningun cliente*/
SELECT Empleados.CodigoEmpleado, Empleados.Nombre, Empleados.Apellido1, Empleados.Puesto FROM Empleados LEFT JOIN Clientes ON Empleados.CodigoEmpleado = Clientes.CodigoEmpleadoRepVentas WHERE Clientes.CodigoEmpleadoRepVentas IS NULL;

/*Sacar listado con el nombre de cada cliente y el nombre y apellidos de su representante de ventas*/
SELECT Clientes.NombreCliente, Empleados.Nombre, Empleados.Apellido1 FROM Clientes, Empleados WHERE Clientes.CodigoEmpleadoRepVentas = Empleados.CodigoEmpleado;
SELECT Clientes.NombreCliente, Empleados.Nombre, Empleados.Apellido1 FROM Clientes JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas = Empleados.CodigoEmpleado; /*Optimizado*/

/*Mostrar el nombre de los clientes que no hayan realizado pagos junto con el nombre de sus representantes de ventas*/
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, Empleados.Nombre AS NombreRepVentas FROM Clientes NATURAL LEFT JOIN Pagos JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas = Empleados.CodigoEmpleado WHERE Pagos.CodigoCliente IS NULL;

/*Listar las ventas totales de los productos que hayan facturado mas de 3000 euros. Mostrar el nombre, unidades vendidas, total facturado y total facturado con impuestos (21%IVA)*/
SELECT Productos.Nombre, DetallePedidos.CodigoProducto, SUM(DetallePedidos.Cantidad), SUM(DetallePedidos.Cantidad * DetallePedidos.PrecioUnidad), SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad)+1.21 FROM DetallePedidos NATURAL JOIN Productos GROUP BY DetallePedidos.CodigoProducto HAVING SUM(DetallePedidos.Cantidad*DetallePedidos.PrecioUnidad)>3000;

/*Listar la direccion de oficinas que tengan clientes en fuenlabrada*/
SELECT Clientes.CodigoCliente, Oficinas.LineaDireccion1 FROM Clientes Join Empleados NATURAL JOIN Oficinas ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado WHERE Clientes.Ciudad='Fuenlabrada';

/*Obtener el producto mas caro*/
SELECT Productos.CodigoProducto, Productos.Nombre, Productos.PrecioVenta FROM Productos WHERE Productos.PrecioVenta=(SELECT MAX(PrecioVenta) FROM Productos);

/*Obtener el nombre del producto del que más unidades se hayan vendido en un mismo pedido*/
SELECT DetallePedidos.CodigoProducto, Productos.Nombre, DetallePedidos.Cantidad FROM DetallePedidos NATURAL JOIN Productos WHERE DetallePedidos.Cantidad = (SELECT MAX(DetallePedidos.Cantidad) FROM DetallePedidos);

/*Obtener el nombre del producto del que más unidades se hayan vendido*/


/*Obtener los clientes cuya linea de credito sea mayor que los pagos que ha realizado*/
SELECT Clientes.CodigoCliente, Clientes.LimiteCredito, SUM(Pagos.Cantidad) FROM Clientes NATURAL JOIN Pagos GROUP BY Clientes.CodigoCliente HAVING Clientes.LimiteCredito > SUM(Pagos.Cantidad);

/*Sacar el producto que mas unidades tiene en stock y el que menos*/
SELECT Productos.CodigoProducto, Productos.Nombre, Productos.CantidadEnStock FROM Productos WHERE CantidadEnStock=(SELECT MAX(Productos.CantidadEnStock) FROM Productos) OR CantidadEnStock=(SELECT MIN(Productos.CantidadEnStock) FROM Productos);

/*Sacar el nombre de los clientes y de sus representen junto con la ciudad a la que pertenece el representante*/
SELECT Clientes.NombreCliente, CONCAT(Empleados.Nombre, ' ',Empleados.Apellido1) AS NombreEmpleado, Oficinas.Ciudad FROM Clientes LEFT JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas = Empleados.CodigoEmpleado LEFT JOIN Oficinas ON Empleados.CodigoOficina = Oficinas.CodigoOficina;

/*Sacar la misma informacion que la consulta anterior pero solo los clientes que no hayan hecho pagos*/
SELECT Clientes.NombreCliente, CONCAT(Empleados.Nombre, ' ',Empleados.Apellido1) AS NombreEmpleado, Oficinas.Ciudad FROM Pagos NATURAL RIGHT JOIN Clientes JOIN Empleados NATURAL JOIN Oficinas ON Clientes.CodigoEmpleadoRepVentas = Empleados.CodigoEmpleado WHERE Pagos.CodigoCliente IS NULL;

/*Obtener un listado con el nombre de los empleados junto con el nombre de sus jefes*/
SELECT Curritos.CodigoEmpleado, Curritos.Nombre, Curritos.Apellido1, Curritos.CodigoJefe, Jefes.CodigoEmpleado, Jefes.Nombre, Jefes.Apellido1 FROM Empleados AS Curritos LEFT JOIN Empleados AS Jefes ON Curritos.CodigoJefe = Jefes.CodigoEmpleado;

/*Obtener el nombre de los clientes a los que no se les ha entregado a tiempo un pedido*/
SELECT Clientes.NombreCliente FROM Clientes WHERE Clientes.CodigoCliente IN (SELECT DISTINCT CodigoCliente FROM Pedidos WHERE FechaEntrega <= FechaEsperada);

/*Sacar un listado de clientes indicando el nombre del cliente y cuantos pedidos a realizado*/

/*Sacar un listado de los nombres de clientes y el total pagado por cada uno de ellos*/

/*Sacar el nombre de los clientes que hayan hecho pedidos en 2008*/

/*Listar el nombre del cliente y el nombre y apellidos de sus representantes de ventas de aquellos clientes que no hayan realizado pagos*/

/*Sacar un listado de clientes donde aparece el nombre de su comercial y la ciudad donde esta su oficina*/

/*Sacar el nombre apellidos oficina y cargo de aquellos que no sean representantes de ventas*/

