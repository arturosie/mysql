use jardineria;

/*Obtener un listado con el nombre de los empleados junto con el nombre de sus jefes*/
SELECT Curritos.CodigoEmpleado, Curritos.Nombre, Curritos.Apellido1, Curritos.CodigoJefe, Jefes.CodigoEmpleado, Jefes.Nombre, Jefes.Apellido1 FROM Empleados AS Curritos LEFT JOIN Empleados AS Jefes ON Curritos.CodigoJefe = Jefes.CodigoEmpleado;

/*Obtener cuantos empleados tiene cada jefe a su cargo*/
SELECT Jefes.Nombre, Jefes.Apellido1, COUNT(Curritos.Nombre) AS 'Curritos' FROM Empleados AS Curritos LEFT JOIN Empleados AS Jefes ON Curritos.CodigoJefe = Jefes.CodigoEmpleado GROUP BY Jefes.CodigoEmpleado;

/*Mostrar todos los nombre del cliente, el del empleado que represente al cliente, el numero de pedidos realizados, que cantidad hay pagada*/
SELECT Clientes.CodigoCliente, Clientes.NombreCliente, CONCAT(Empleados.Nombre, Empleados.Apellido1) AS NombreEmpleado, SUM(Pagos.Cantidad) AS CantidadPagada, COUNT(Pedidos.CodigoCliente) AS NºPedidos FROM Clientes LEFT JOIN (Empleados, Pagos, Pedidos) On (Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado AND Clientes.CodigoCliente = Pagos.CodigoCliente AND Clientes.CodigoCliente = Pedidos.CodigoCliente) GROUP BY Clientes.CodigoCliente; /*No esta terminada*/

/*Preguntar*/
INSERT INTO ClientesVIP SELECT Clientes.CodigoCliente, SUM(DetallePedidos.PrecioUnidad*DetallePedidos.Cantidad) FROM Clientes NATURAL JOIN Pedidos NATURAL JOIN DetallePedidos GROUP BY Clientes.CodigoCliente HAVING SUM(DetallePedidos.PrecioUnidad*DetallePedidos.Cantidad)>3000;

/*Preguntas la diferencia con la de arriba*/
CREATE VIEW ClientesPedidosGastado AS SELECT Clientes.CodigoCliente, SUM(DetallePedidos.PrecioUnidad*DetallePedidos.Cantidad) FROM Clientes NATURAL JOIN Pedidos NATURAL JOIN DetallePedidos GROUP BY Clientes.CodigoCliente HAVING SUM(DetallePedidos.PrecioUnidad*DetallePedidos.Cantidad)>3000;
