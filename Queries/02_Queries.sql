use jardineria;

/*Mostrar codigo y nombre del cliente y del pedido que ha realizado*/
SELECT Clientes.NombreCliente, Clientes.CodigoCliente, Pedidos.CodigoCliente, Pedidos.CodigoPedido FROM Clientes, Pedidos WHERE Clientes.CodigoCliente=Pedidos.CodigoCliente;

/*Mostrar codigo y nombre del cliente y del pedido que ha realizado*/  /* LO MISMO QUE LA DE ARRIBA PERO OPTIMIZADA USANDO JOIN */
SELECT Clientes.NombreCliente, Clientes.CodigoCliente, Pedidos.CodigoPedido FROM Clientes JOIN Pedidos ON Clientes.CodigoCliente=Pedidos.CodigoCliente;

/*Mostrar codigo y nombre del cliente y del pedido que ha realizado*/  /* LO MISMO QUE LA DE ARRIBA PERO OPTIMIZADA USANDO JOIN */
SELECT Clientes.NombreCliente, Clientes.CodigoCliente, Pedidos.CodigoPedido FROM Clientes NATURAL JOIN Pedidos;

/*Mostrar Nombre del empleado y su codigo, el codigo de la oficina y su nombre y ciudad de la oficina*/
SELECT Empleados.Nombre, Empleados.CodigoEmpleado, Oficinas.CodigoOficina, Oficinas.Ciudad FROM Empleados NATURAL JOIN Oficinas;

/*Mostrar el codigo y nombre del cliente y del empleado*/
SELECT Empleados.Nombre, Empleados.CodigoEmpleado, Clientes.CodigoCliente, Clientes.NombreCliente FROM Clientes JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado;

/*Mostrar el codigo y el nombre del cliente de todos los clientes tengan o no representante de ventas con el codigo y el nombre del empleado*/
SELECT Empleados.Nombre, Empleados.CodigoEmpleado, Clientes.CodigoCliente, Clientes.NombreCliente FROM Clientes LEFT OUTER JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado;

/*Mostrar el codigo y el nombre del cliente de todos los clientes tengan o no representante de ventas con el codigo y el nombre del empleado tengan o no clientes*/
SELECT Empleados.Nombre, Empleados.CodigoEmpleado, Clientes.CodigoCliente, Clientes.NombreCliente FROM Clientes RIGHT OUTER JOIN Empleados ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado;

/*Mostrar por cada empleado cuantos clientes representa*/
SELECT Empleados.CodigoEmpleado, Empleados.Nombre, COUNT(Clientes.CodigoEmpleadoRepVentas) AS 'Numero Clientes' FROM Empleados LEFT JOIN Clientes ON Clientes.CodigoEmpleadoRepVentas=Empleados.CodigoEmpleado GROUP BY Empleados.CodigoEmpleado;

/*Mostrar la categoria que no tiene ningun producto*/
SELECT Productos.CodigoProducto, Productos.Nombre, GamasProductos.* FROM Productos NATURAL RIGHT JOIN GamasProductos WHERE Productos.CodigoProducto IS NULL;
