use jardineria;

/* Mostrar Codigo, nombre y tipo de los productos*/
SELECT CodigoProducto AS Codigo, Nombre, Gama AS Tipo FROM Productos;

/* Mostrar los productos distintos que existen*/
SELECT DISTINCT Nombre FROM Productos;

/* Ver Productos con el precio mayor que 25*/
SELECT Nombre, PrecioVenta FROM Productos WHERE PrecioVenta>25;

/* Ver el producto mas caro*/
SELECT * FROM Productos ORDER BY PrecioVenta DESC LIMIT 1;

/* Productos que empiezen por M (no por m)*/
SELECT DISTINCT Nombre FROM Productos WHERE Nombre LIKE BINARY 'M%';

/* Mostrar productos que contienen la palabra "Semilla"*/
SELECT Nombre FROM Productos WHERE Nombre LIKE '%Semilla%';

/* Ver que productos se han entregado a tiempo*/
SELECT * FROM Pedidos WHERE FechaEsperada>=FechaEntrega;

/* Mostrar pedidos sin entregar*/
SELECT * FROM Pedidos WHERE FechaEntrega is NULL;

/* Mostrar pedidos contando el numero de dias de retraso del pedido*/
SELECT CodigoPedido, FechaPedido, FechaEntrega, TIMESTAMPDIFF(DAY, FechaEsperada, FechaEntrega) AS Dias_retrasados FROM Pedidos WHERE FechaEsperada<FechaEntrega;

/* Mostrar productos que su codigo empieza por 11 o 21 o 22*/
SELECT CodigoProducto, Nombre, Gama FROM Productos WHERE CodigoProducto LIKE '11%' OR CodigoProducto LIKE '21%' OR CodigoProducto LIKE '22%';

/* Mostrar productos que su codigo no empieza por dos letras*/
SELECT CodigoProducto, Nombre, Gama FROM Productos WHERE CodigoProducto BETWEEN '00%' AND '99%';

/* Mostrar clientes que su ciudad sea Madrid o Fuenlabrada o Barcelona*/
SELECT CodigoCliente, Ciudad FROM Clientes WHERE Ciudad IN ('Madrid', 'Fuenlabrada', 'Barcelona');

/* Mostrar productos que su precio este entre 20 y 40*/
SELECT CodigoProducto, Nombre, Gama, PrecioVenta FROM Productos WHERE PrecioVenta BETWEEN '20' AND '40';

/* Mostrar cantidad de unidades de cada tipo*/
SELECT Gama, COUNT(*) AS 'Unidades' FROM Productos GROUP BY Gama;

/*Mostrar el codigo de oficina y la ciudad donde hay oficinas*/
SELECT CodigoOficina, Ciudad FROM Oficinas;

/*Mostrar cuantos empleados hay en la compañia*/
SELECT CodigoEmpleado AS 'Numero empleados' FROM Empleados;

/*Mostrar cuantos clientes tiene cada pais*/
SELECT Pais, COUNT(*) AS 'Numero de clientes' FROM Clientes GROUP BY Pais;

/*Mostrar la media de pago de cada año*/
SELECT YEAR(FechaPago), AVG(Cantidad) FROM Pagos GROUP BY YEAR(FechaPago);

/*Mostrar cual fue el pago medio en 2009*/
SELECT AVG (Cantidad) AS 'Pago medio' FROM Pagos WHERE YEAR(FechaPago)=2009;

/*Mostrar la media de pago de cada mes en */
SELECT MONTH(FechaPago), AVG(Cantidad) FROM Pagos WHERE YEAR(FechaPago)=2009 GROUP BY MONTH(FechaPago);

/*Mostrar el precio de cada pedido*/
SELECT CodigoPedido, SUM(Cantidad * PrecioUnidad) AS 'Precio total' FROM DetallePedidos GROUP BY CodigoPedido;

/*Mostrar el pedido más caro*/
SELECT CodigoPedido, SUM(Cantidad * PrecioUnidad) AS PrecioTotal FROM DetallePedidos GROUP BY CodigoPedido ORDER BY PrecioTotal DESC LIMIT 1;

/*Mostrar ciudad y telefono de oficinas de EE.UU*/
SELECT Telefono, Ciudad FROM Oficinas WHERE Pais='EEUU';

/*Mostrar el nombre, apellidos y el email de los empleados a cargo de "Alberto Soria"*/
SELECT Nombre, CONCAT(Apellido1, ' ', Apellido2) AS 'Nombre completo', Email FROM Empleados WHERE CodigoJefe=3;

/*Mostrar el cargo, nombre, apellidos y email del jefe de la empresa*/
SELECT Puesto, CONCAT(Nombre, ' ', Apellido1) AS 'Nombre completo', Email FROM Empleados WHERE CodigoJefe IS NULL;

/*Mostrar nombre, apellidos y cargo de aquellos que no sean representantes de ventas*/
SELECT CodigoEmpleado, CONCAT(Nombre, ' ', Apellido1) AS 'Nombre completo', Puesto FROM Empleados WHERE Puesto!='Representante Ventas';

/*Mostrar el numero de clientes que tiene la empresa*/
SELECT COUNT(*) AS 'Total clientes' FROM Clientes;

/*Mostrar paises que su numero de clientes sea mayor que 3*/
SELECT Pais, COUNT(*) AS NumeroClientes FROM Clientes GROUP BY Pais HAVING NumeroClientes >3;
