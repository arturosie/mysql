
/* PROCEDURE return num to Clientes, Empleados and Pedidos */

DROP PROCEDURE IF EXISTS crearInfo;

DELIMITER |

CREATE PROCEDURE crearInfo()
BEGIN
    DROP TABLE IF EXISTS Info;
    CREATE TABLE Info (NumClientes INT, NumEmpleados INT, NumPedidos INT);
      INSERT INTO Info VALUES (
                                (SELECT COUNT(*) FROM Clientes),
                                (SELECT COUNT(*) FROM Empleados),
                                (SELECT COUNT(*) FROM Pedidos)
                              );
END;

 |

DELIMITER ;

/*CALL crearInfo(); Llamada a procedimiento*/


/* PROCEDURE return num to Clientes, Empleados and Pedidos */

DROP PROCEDURE IF EXISTS crearInfov2;

DELIMITER |

CREATE PROCEDURE crearInfov2()

BEGIN

    DECLARE NumClientes,NumEmpleados,NumPedidos INT;

    DROP TABLE IF EXISTS Info2;

    CREATE TABLE Info2 (NumClientes INT, NumEmpleados INT, NumPedidos INT);

    SET NumClientes   = (SELECT COUNT(*) FROM Clientes);
    SET NumEmpleados  = (SELECT COUNT(*) FROM Empleados);
    SET NumPedidos    = (SELECT COUNT(*) FROM Pedidos);

    INSERT INTO Info VALUES (NumClientes, NumEmpleados, NumPedidos);

END;

 |

DELIMITER ;

/*CALL crearInfov2(); Llamada a procedimiento*/

