
/* AUTORES: Arturo Sierra Sánchez y Miguel Ángel López Hernández */

USE jardineria;
DROP TABLE IF EXISTS Facturas;
DROP TABLE IF EXISTS Comisiones;
DROP TABLE IF EXISTS AsientoContable;
DROP TABLE IF EXISTS ActualizacionLimiteCredito;

CREATE TABLE ActualizacionLimiteCredito (
    CodigoCliente INT,
    Fecha DATE,
    Incremento NUMERIC(15,2),

    CONSTRAINT ACTUA_CLIENTESFK FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente)
);


CREATE TABLE Facturas (
    CodigoFactura INT,
    CodigoCliente INT,
    BaseImponible NUMERIC(15,2),
    IVA NUMERIC(15,2) DEFAULT '21' COMMENT '%',
    Total NUMERIC(15,2),

    CONSTRAINT FACTURAS_PEDIDOFK FOREIGN KEY (CodigoFactura) REFERENCES Pedidos(CodigoPedido),
    CONSTRAINT FACTURAS_CLIENTESFK FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente)
);


CREATE TABLE Comisiones (
    CodigoFactura INT,
    CodigoEmpleado INT,
    Comision NUMERIC(15,2),

    CONSTRAINT COMISIONES_FACTURASFK FOREIGN KEY (CodigoFactura) REFERENCES Facturas(CodigoFactura)
);

CREATE TABLE AsientoContable (
    CodigoCliente INT,
    DEBE NUMERIC(15,2),
    HABER NUMERIC(15,2),

    CONSTRAINT ASIE_CLIENTESFK FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente)
);

