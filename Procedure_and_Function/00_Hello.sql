/* PROCEDURE  return Hola + name */
DROP PROCEDURE IF EXISTS hola;

delimiter //

CREATE PROCEDURE hola (IN nombre VARCHAR(20))
BEGIN
    SELECT (CONCAT('Hola, ',nombre)) AS 'Terminal - procedure';
END//

delimiter ;

CALL hola('Arturo'); /*Llamada a procedimiento*/

/*FUNCTION  return Hola + name */

DROP FUNCTION IF EXISTS hola;

CREATE FUNCTION hola (nombre VARCHAR(20))
RETURNS VARCHAR(50) DETERMINISTIC
RETURN CONCAT('Hola,',' ',nombre,'!');

SELECT hola('Arturo') AS 'Terminal - function'; /*Llamada a funcion*/


/* PROCEDURE return n */

DROP PROCEDURE IF EXISTS p;

DELIMITER |

CREATE PROCEDURE p(IN n INT)
BEGIN

    CASE n
      WHEN 2 THEN SELECT n;
      WHEN 3 THEN SELECT 0;
      ELSE
       BEGIN
       END;
    END CASE;
END;

 |

CALL p(2); /*Llamada a procedimiento*/


/* PROCEDURE return month */

DROP PROCEDURE IF EXISTS month;

DELIMITER |

CREATE PROCEDURE month(IN n INT)
BEGIN

    CASE n
      WHEN 1 THEN SELECT 'Enero' AS 'Month';
      WHEN 2 THEN SELECT 'Febrero' AS 'Month';
      WHEN 3 THEN SELECT 'Marzo' AS 'Month';
      WHEN 4 THEN SELECT 'Abril' AS 'Month';
      WHEN 5 THEN SELECT 'Mayo' AS 'Month';
      WHEN 6 THEN SELECT 'Junio' AS 'Month';
      WHEN 7 THEN SELECT 'Julio' AS 'Month';
      WHEN 8 THEN SELECT 'Agosto' AS 'Month';
      WHEN 9 THEN SELECT 'Septiembre' AS 'Month';
      WHEN 10 THEN SELECT 'Octubre' AS 'Month';
      WHEN 11 THEN SELECT 'Noviembre' AS 'Month';
      WHEN 12 THEN SELECT 'Diciembre' AS 'Month';

      ELSE
       BEGIN
       END;
    END CASE;
END;

 |

CALL month(2); /*Llamada a procedimiento*/


/* PROCEDURE return month */

DROP PROCEDURE IF EXISTS monthv2;

DELIMITER |

CREATE PROCEDURE monthv2(IN n INT)
BEGIN

  WHILE n < 13 DO
    CASE n
      WHEN 1 THEN SELECT 'Enero' AS 'Month';
      WHEN 2 THEN SELECT 'Febrero' AS 'Month';
      WHEN 3 THEN SELECT 'Marzo' AS 'Month';
      WHEN 4 THEN SELECT 'Abril' AS 'Month';
      WHEN 5 THEN SELECT 'Mayo' AS 'Month';
      WHEN 6 THEN SELECT 'Junio' AS 'Month';
      WHEN 7 THEN SELECT 'Julio' AS 'Month';
      WHEN 8 THEN SELECT 'Agosto' AS 'Month';
      WHEN 9 THEN SELECT 'Septiembre' AS 'Month';
      WHEN 10 THEN SELECT 'Octubre' AS 'Month';
      WHEN 11 THEN SELECT 'Noviembre' AS 'Month';
      WHEN 12 THEN SELECT 'Diciembre' AS 'Month';

      ELSE
       BEGIN
       END;
    END CASE;
    SET n = n + 1;
  END WHILE;
END;

 |

CALL monthv2(2); /*Llamada a procedimiento*/

/* PROCEDURE return nam */

DROP PROCEDURE IF EXISTS crearInfo;

DELIMITER |

CREATE PROCEDURE crearInfo()
BEGIN
    DROP TABLE IF EXISTS Info;
    CREATE TABLE Info (NumClientes INT, NumEmpleados INT, NumPedidos INT);
END;

 |

CALL crearInfo(); /*Llamada a procedimiento*/

