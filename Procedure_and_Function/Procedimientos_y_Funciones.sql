
/* AUTORES: Arturo Sierra Sánchez y Miguel Ángel López Hernández */

USE jardineria;

/*  Actualiza limite de credito de los clientes en un 15%, además, guarda los datos en tabla ActualizacionLimiteCredito  */
DROP PROCEDURE IF EXISTS ActualizaLimite;

DELIMITER |

CREATE PROCEDURE ActualizaLimite()
BEGIN

    /*Declaramos varibales*/
    DECLARE   FinCliente      INT;
    DECLARE   i               INT;
    DECLARE   Porcentaje      NUMERIC(15,2);
    DECLARE   Fecha           DATE;
    DECLARE   IDCliente       INT;
    DECLARE   AumentaLimite   NUMERIC(15,2);

    SET FinCliente  =   (SELECT COUNT(*) FROM Clientes ORDER BY CodigoCliente); /*Total clientes*/
    SET Fecha       =   (SELECT CURDATE()); /*Fecha actual (YYYY-MM-DD)*/

    SET i = 1;

    while i<= FinCliente do

    /*Codigo de todos los clientes*/
    SET IDCliente = (SELECT CodigoCliente FROM Clientes WHERE CodigoCliente = i ORDER BY CodigoCliente);

    /*Si no existe el cliente no se realiza el calculo de gasto entre 2008 y 2010*/
    IF IDCLIENTE IS NOT NULL then
      SET Porcentaje = (SELECT SUM(Cantidad)*0.15 AS 'Cantiadad 15%' FROM Pagos WHERE CodigoCliente = i AND YEAR(FechaPago) BETWEEN 2008 AND 2010 GROUP BY CodigoCliente);
    end IF;

    /*Si no ha realizado pedidos el aumento de credito sera 0*/
    IF Porcentaje IS NULL then
      SET Porcentaje = 0;
    end IF;

    /*Guardamos datos en la tabla*/
    IF IDCLIENTE IS NOT NULL then
      INSERT INTO ActualizacionLimiteCredito VALUES (IDCliente, Fecha, Porcentaje);
    end IF;

    /*Obtenemos el aumento de credito del cliente*/
    IF IDCliente IS NOT NULL then
      SET AumentaLimite = (SELECT Incremento FROM ActualizacionLimiteCredito WHERE CodigoCliente = i);
    end IF;

    /*Aumentamos el limite de credito (sumamos el incremento + limite de credito)*/
    IF IDCLIENTE IS NOT NULL then
      UPDATE Clientes SET LimiteCredito = LimiteCredito + Porcentaje WHERE CodigoCliente = i;
    end IF;

    /*Aumentamos i para realizar de nuevo el bucle*/
    SET i = i+1;

    end while;

END;

|

DELIMITER ;

CALL ActualizaLimite();


/* Funcion muestra la ultima factura y en caso de no existir ninguna factura devolverá 1*/

DROP FUNCTION IF EXISTS UltimaFactura;

DELIMITER |

CREATE FUNCTION UltimaFactura ()

RETURNS INT
  BEGIN
    DECLARE UltimaFactura INT;

    SET UltimaFactura = (SELECT MAX(CodigoFactura) FROM Facturas);  /* Comprobamos si existe Facturas, y si existe, cual es su Codigo */

    /* Si no existe ninguna factura valdra 1*/
    IF UltimaFactura IS NULL then
      SET UltimaFactura = 1;
    end IF;

    /* Variable que devuelve la funcion (ultima factura) */
    RETURN UltimaFactura;
  END
|
DELIMITER ;

SELECT UltimaFactura() AS 'Ultima factura';


/* Procedimiento crea una factura por pedido que exista y crea una comision del 5% para el empleado */

DROP PROCEDURE IF EXISTS Facturar;

DELIMITER |

CREATE PROCEDURE Facturar()
BEGIN

  /* Declaracion de variables */
  DECLARE   CodigoClientePedido   INT;
  DECLARE   TotalFactura          NUMERIC(15,2);
  DECLARE   PrecioBaseImponible   NUMERIC(15,2);
  DECLARE   FinCliente            INT;
  DECLARE   FinPedidoCliente      INT;
  DECLARE   FacturaCodigo         INT;
  DECLARE   ComisionEmpleado      INT;
  DECLARE   ComisionFactura       INT;
  DECLARE   ComisionIncremento    NUMERIC(15,2);
  DECLARE   i                     INT;
  DECLARE   j                     INT;

  SET i = 1;
  SET j = 1;
  SET FinCliente        =   (SELECT COUNT(*) FROM Pedidos);           /* Numero de Pedidos */
  SET FinPedidoCliente  =   (select MAX(CodigoPedido) FROM Pedidos);  /* Codigo Pedido mayor */

  /* Bucle incrementa CodigoCliente */
  while i<= FinPedidoCliente do

    /* Bucle incrementa CodigoPedido */
    while j <= FinPedidoCliente do

      /* Obtenemos datos para insertarlos en la tabla Facturas */
      SET FacturaCodigo         =   (SELECT CodigoPedido FROM Pedidos WHERE CodigoCliente = i && CodigoPedido = j);                               /* CodigoFactura de tabla Pedidos */
      SET CodigoClientePedido   =   (SELECT CodigoCliente FROM Pedidos WHERE CodigoCliente = i && CodigoPedido = j);                              /* CodigoCliente de la tabla Pedidos */
      SET TotalFactura          =   (SELECT SUM(Cantidad * PrecioUnidad) AS 'Precio total' FROM DetallePedidos WHERE CodigoPedido = j);           /* Precio total del pedido realizado */
      SET PrecioBaseImponible   =   (SELECT SUM((Cantidad * PrecioUnidad) / 1.21) AS 'Precio total' FROM DetallePedidos WHERE CodigoPedido = j);  /* Precio sin IVA del pedido realizado */

        IF TotalFactura IS NOT NULL then
            IF CodigoClientePedido IS NOT NULL then

              /* Inserta datos en tabla Facturas */
              INSERT INTO Facturas (CodigoFactura, CodigoCliente, BaseImponible, Total) VALUES (FacturaCodigo, CodigoClientePedido, PrecioBaseImponible, TotalFactura);

              /* Obtenemos datos para insertarlos en la tabla Comisiones */
              SET ComisionEmpleado = (SELECT CodigoEmpleadoRepVentas FROM Clientes WHERE CodigoCliente = CodigoClientePedido);
              SET ComisionIncremento = (SELECT SUM((Cantidad * PrecioUnidad)*0.05) AS 'Precio total' FROM DetallePedidos WHERE CodigoPedido = j);

              IF ComisionIncremento IS NOT NULL then
                INSERT INTO Comisiones(CodigoFactura, CodigoEmpleado, Comision) VALUES (FacturaCodigo, ComisionEmpleado, ComisionIncremento);
              end IF;

            end IF;

        end IF;

      /*Si el codigo del cliente es null no introducimos datos en tabla facturas*/
      SET j = j + 1;

    /*Fin bucle CodigoPedido*/
    end while;

    /*Si j es mayor a el numero de pedidos vuelve a valer 1*/
    IF j > FinCliente then
        SET j = 1;
    end IF;

    SET i = i + 1;

  /*Fin bucle CodigoCliente*/
  end while;

END;

|

DELIMITER ;

CALL Facturar();


/* Procedimiento que genera el Deber y el Haber en cada cliente */

DROP PROCEDURE IF EXISTS GenerarContabilidad;

DELIMITER |

CREATE PROCEDURE GenerarContabilidad()
BEGIN

  DECLARE   i             INT;
  DECLARE   NClientes     INT;
  DECLARE   Cliente       INT;
  DECLARE   GastoCliente  NUMERIC(15,2);

  SET i = 0;
  SET NClientes = (SELECT COUNT(*) FROM Clientes);

  while i<= NClientes do
    /* Obtenemos el codigo del cliente */
    SET Cliente = (SELECT CodigoCliente FROM Clientes WHERE CodigoCliente = i);

    /* Si existe un cliente con ese codigo */
    IF Cliente IS NOT NULL then
      /* Suma del total gastado en todos los pedidos del cliente */
      SET GastoCliente = (SELECT SUM(Total) AS 'Precio total' FROM Facturas WHERE CodigoCliente = i GROUP BY CodigoCliente);

      /* Si el cliente ha realizado gastos */
      IF GastoCliente IS NOT NULL then
        /* Insertamos datos en tabla AsientoContable */
        INSERT INTO AsientoContable(CodigoCliente, DEBE, HABER) VALUES (Cliente, GastoCliente, GastoCliente);
      end IF;

    end IF;

    SET i = i + 1;
  end while;

END;

|

DELIMITER ;

CALL GenerarContabilidad();


/* Procedimiento que obtiene un ranking de los empleados y sus clientes */

DROP PROCEDURE IF EXISTS RankingEmpleados;

DELIMITER |

CREATE PROCEDURE RankingEmpleados()
BEGIN

  SELECT Empleados.CodigoEmpleado, CONCAT(Empleados.Nombre,' ',Empleados.Apellido1) AS 'Empleado', COUNT(Clientes.CodigoCliente) AS 'NumeroClientes' FROM Clientes JOIN Empleados ON Empleados.CodigoEmpleado = Clientes.CodigoEmpleadoRepVentas GROUP BY Empleados.CodigoEmpleado ORDER BY NumeroClientes DESC;

END;

|

DELIMITER ;

CALL RankingEmpleados();


/* Procedimiento que obtiene una media de los productos que hay en un pedido */

DROP PROCEDURE IF EXISTS MediaProductos;

DELIMITER |

CREATE PROCEDURE MediaProductos()
BEGIN

    /* Media de productos por cada pedido */
    SELECT CodigoPedido, AVG(Cantidad) FROM DetallePedidos GROUP BY CodigoPedido;

END;

|

DELIMITER ;

CALL MediaProductos();
